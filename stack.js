
class Stack {
    constructor() {
        this.data = [];
        this.size = 0;
    }

    push(element) {
        this.data[this.size] = element;
        this.size++;
        return this.data[this.size - 1];
    }

    length() {
        return this.size;
    }

    peek() {
        if (this.isEmpty()) throw new Error('Stack is empty');
        return this.data[this.size - 1];
    }

    isEmpty() {
        return this.size === 0;
    }

    pop() {
        if (this.isEmpty()) throw new Error('Stack is empty');
        var value = this.peek();
        this.size = this.size - 1;
        delete this.data[this.size];
        return value;
    }

    clear() {
        this.size = 0;
        this.data = [];
    }

    print() {
        console.log(this.data.toString());
    }

}

module.exports = new Stack();