const stack = require('../stack');

beforeEach(() => {
    stack.clear();
});

test('Test isEmpty when stack is not empty', () => {
    stack.push('element');
    expect(stack.isEmpty()).toBe(false);
});

test('Test isEmpty when stack is empty', () => {
    expect(stack.isEmpty()).toBe(true);
});

test('Test isEmpty when stack goes from empty to non-empty', () => {
    expect(stack.isEmpty()).toBe(true);
    stack.push("a");
    expect(stack.isEmpty()).toBe(false);
});

test('Test isEmpty when stack goes from non-empty to empty', () => {
    stack.push("a");
    expect(stack.isEmpty()).toBe(false);
    stack.pop();
    expect(stack.isEmpty()).toBe(true);
});

test('Test isEmpty when pop empty stack', () => {
    try {
        stack.pop();
    } catch (e) {

    }
    expect(stack.isEmpty()).toBe(true);
});