const stack = require('../stack');

beforeEach(() => {
    stack.clear();
});

test('Test length when stack is empty', () => {
    expect(stack.length()).toBe(0);
});

test('Test length when stack is not empty', () => {
    var expectedLength = 10
    for (var i = 0; i < expectedLength; i++)
        stack.push(i);
    expect(stack.length()).toBe(expectedLength);
});

test('Test length when stack is not empty when push then pop', () => {
    var expectedLength = 5
    for (var i = 0; i < expectedLength * 2; i++)
        stack.push(i);

    for (var i = 0; i < expectedLength; i++)
        stack.pop();
    expect(stack.length()).toBe(expectedLength);
});

test('Test length when pop empty stack', () => {
    try {
        stack.pop();
    } catch (e) {

    }
    expect(stack.length()).toBe(0);
});