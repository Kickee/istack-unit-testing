const stack = require('../stack');

beforeEach(() => {
    stack.clear();
});

test('Test pop empty stack', () => {
    const pop = () => {
        stack.pop();
    }
    expect(pop).toThrow(Error);
});

test('Test pop consecutive', () => {
    var size = 5;
    for (var i = 0; i < size; i++) {
        stack.push(i);
    }
    for (i = size-1; i>=0; i--) {
        expect(stack.pop()).toBe(i);
    }

    expect( () => {
        stack.pop();
    }).toThrow(Error);
});


// test('Test the validity of function push of stack with a character', () => {
//     expect(stack.push('a')).toBe('a');
// });

// test('Test the validity of function push of stack with a string', () => {
//     expect(stack.push('hello world')).toBe('hello world');
// });