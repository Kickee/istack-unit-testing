const stack = require('../stack');

beforeEach(() => {
    stack.clear();
});

test('Test push return', () => {
    var expected = "expected";
    expect(stack.push(expected)).toBe(expected);
});

test('Test push return after pop', () => {
    stack.push("stuff");
    stack.push("stuff2");
    stack.pop();

    var expected = "expected";
    expect(stack.push(expected)).toBe(expected);
});

test('Test push return after clear', () => {
    stack.push("stuff");
    stack.push("stuff2");
    stack.clear();

    var expected = "expected";
    expect(stack.push(expected)).toBe(expected);
});