const stack = require('../stack');

beforeEach(() => {
    stack.clear();
});

test('Test peek empty stack', () => {
    const peek = () => {
        stack.peek();
    }
    expect(peek).toThrow(Error);
});

test('Test peek freshly inserted element', () => {
    var expectedOutput = "hello";
    stack.push(expectedOutput);
    expect(stack.peek()).toBe(expectedOutput);
});

test('Test peek when stack got pushed then popped', () => {
    var expected1 = "hello";
    var expected2 = "world";
    stack.push(expected1);
    expect(stack.peek()).toBe(expected1);
    stack.push(expected2);
    expect(stack.peek()).toBe(expected2);
    stack.pop(expected2);
    expect(stack.peek()).toBe(expected1);
});